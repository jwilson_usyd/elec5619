package au.usyd.elec5619.domain;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class CreateGroupForm {
	@NotBlank(message = "Please enter a name for the group.")
	private String groupName;

	@NotBlank(message = "Please enter a description.")
	private String description;
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String name) {
		groupName = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String desc) {
		description = desc;
	}
	
	public UserGroup toGroup() {
		UserGroup newGroup = new UserGroup();
		newGroup.setGroupName(groupName);
		newGroup.setDescription(description);
		return newGroup;
	}
}
